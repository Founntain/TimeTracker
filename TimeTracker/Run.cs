﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTracker {
    class Run {
        public String Game { get; set; }
        public String Time { get; set; }
        public List<String> Notes { get; set; }
        public String Folder { get; set; }
        public String Path { get; set; }

        public Run(String game, String time, List<String> notes, String folder, String path) {
            Game = game;
            Time = time;
            Notes = notes;
            Folder = folder;
            Path = path;
        }
    }
}
