﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;


namespace TimeTracker {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        List<String> Dirs = new List<string>();
        List<Run> Runs = new List<Run>();
        Run CurrentRun;

        public MainWindow() {
            InitializeComponent();

            Random rdm = new Random();

            if (!Directory.Exists("data")) {
                Directory.CreateDirectory("data");
            }

            RefreshList();
        }

        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            DragMove();
        }

        private void close_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            Close();
        }

        private void minimize_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            WindowState = WindowState.Minimized;
        }

        private void gameBox_GotFocus(object sender, RoutedEventArgs e) {

            if(gameBox.Text != "Game") {
                return;
            }

            gameBox.Text = "";
        }

        private void timeBox_GotFocus(object sender, RoutedEventArgs e) {

            if(timeBox.Text != "Time: hh:mm:ss.ms") {
                return;
            }

            timeBox.Text = "";
        }

        void RefreshList() {
            timeList.Items.Clear();
            Runs.Clear();

            Dirs = Directory.GetDirectories("data/").ToList();

            if(Dirs.Count <= 0) {
                MessageBox.Show("No times found in data folder.", "No times found");
                return;
            }

            foreach(String s in Dirs) {
                var data = File.ReadAllLines(s + "/info.txt");

                timeList.Items.Add(data[0]+" | "+data[1]);

                Runs.Add(new Run(data[0], data[1], File.ReadAllLines(s + "/notes.txt").ToList(), Path.GetFileName(s), Path.GetFullPath(s)));
            }
        }

        private void saveBtn_Click(object sender, RoutedEventArgs e) {
            String game = gameBox.Text;
            String time = timeBox.Text;

            int id = new Random().Next(10000, 100000);

            List<String> note = new List<String>();

            for(int i = 0; i<noteBox.LineCount; i++) {
                note.Add(noteBox.GetLineText(i));
            }

            Directory.CreateDirectory("data/"+id + "_" + game);
            File.WriteAllLines("data/"+ id + "_" + game + "/info.txt",new string[] { game, time });
            File.WriteAllLines("data/" + id + "_" + game + "/notes.txt", note);

            RefreshList();

            gameBox.Text = "Game";
            timeBox.Text = "Time: hh:mm:ss.ms";
            noteBox.Clear();
        }

        private void timeList_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            LoadRun();
        }

        private void timeList_MouseDoubleClick(object sender, MouseButtonEventArgs e) {
            LoadRun();
        }

        void LoadRun() {
            infoGrid.Visibility = Visibility.Visible;

            int i = timeList.SelectedIndex;

            Console.WriteLine("INDEX: "+i);

            if (i < 0) {
                return;
            }

            gameInfo.Text = Runs[i].Game;
            timeInfo.Text = "Time: " + Runs[i].Time;
            pathInfo.Text = "Folder: " + Runs[i].Folder;

            noteInfoBox.Clear();

            foreach (String s in Runs[i].Notes) {
                if (s.Equals("\n") || s == String.Empty || s.Equals(" ")) {
                    continue;
                }
                noteInfoBox.AppendText(s + "\n");
            }

            CurrentRun = Runs[i];
        }

        private void closeTime_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            infoGrid.Visibility = Visibility.Hidden;
        }

        private void pathInfo_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            Console.WriteLine(CurrentRun.Path);
            Process.Start(CurrentRun.Path);
        }

        private void TextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {

            var data = Assembly.GetExecutingAssembly().Location.Split('\\');

            String p = "";

            for(int i = 0; i<=data.Length-2; i++) {
                p = p + data[i] + "\\";
            }

            Process.Start(p);
        }

        private void refreshBtn_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            RefreshList();
        }
    }
}
